#!/usr/bin/env bash 

timestamp="$( date +%y%m%d%H%M  )"
suffix="${1:-cntk}"

for dir in Output/*
do
	nnodes="${dir#Output/}"
	echo "${nnodes} nodes. Moving files..."
	mkdir -p ~/"${suffix}${timestamp}/${nnodes}/"
	mv "Output/${nnodes}/Log_"* ~/"${suffix}${timestamp}/${nnodes}/"

done

echo Done.
